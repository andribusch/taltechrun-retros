## 4. Sprint
### Retro ja sprint

---
## Mis oli plaanis
@ul
- Punktide kogumise süsteem
- Relvaga sihitud laskmine
- Kaardi ääred on fikseeritud
- Dokumentatsiooni tegema hakkamine
@ulend
---
## Mis sai tehtud
@ul
- Boonuspunktide süsteem
- Laskmine
- Dokumentatsiooni 
@ulend
---
## Retro- Milliseid praktikaid jätkata
@ul
- Ühised koosviibimised 
@ulend
---
## Retro - 3 punkti, mida järgmine sprint teisiti teha
@ul
- Igapäevased standup koosolekud 
- Aidata teisi üle probleemidest
- Iga tiimiliige panustab 15h (sprindi jooksul)
@ulend
---
